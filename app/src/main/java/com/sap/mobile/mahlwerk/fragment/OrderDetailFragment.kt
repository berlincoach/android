package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sap.cloud.android.odata.odataservice.Job
import com.sap.cloud.android.odata.odataservice.Order
import com.sap.cloud.android.odata.odataservice.Task
import com.sap.cloud.mobile.odata.DataQuery
import com.sap.mobile.mahlwerk.adapter.OrderEventAdapter
import com.sap.mobile.mahlwerk.extension.orderEventType
import com.sap.mobile.mahlwerk.extension.orderStatus
import com.sap.mobile.mahlwerk.extension.setupActionBar
import com.sap.mobile.mahlwerk.model.OrderEventType
import com.sap.mobile.mahlwerk.screen.OrderScreen
import kotlinx.android.synthetic.main.fragment_order_detail.*

/**
 * This fragment displays the details of an order with a timeline
 */
class OrderDetailFragment : Fragment(), OrderScreen {
    /** Adapter that holds all OrderEvents */
    private lateinit var orderEventAdapter: OrderEventAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            com.sap.mobile.mahlwerk.R.layout.fragment_order_detail,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timelineView_orderDetail.isNestedScrollingEnabled = false
        observeOrder()
    }

    override fun onPause() {
        super.onPause()
        setHasOptionsMenu(false)
        activity?.invalidateOptionsMenu()
    }

    /**
     * Observes the selected order and binds its information to the view
     */
    private fun observeOrder() {
        viewModel.selectedOrder.observe(this, Observer<Order> { order ->
            viewModel.loadProperties(order, Order.customer, Order.orderEvents)

            setupActionBar(toolbar_orderDetail, order.title)

            val dueDate = getString(com.sap.mobile.mahlwerk.R.string.dueDate)
                .format(order.dueDate.month, order.dueDate.day, order.dueDate.year)

            objectHeader_orderDetail.headline = getString(com.sap.mobile.mahlwerk.R.string.customerName)
                .format(order.customer.companyName)
            objectHeader_orderDetail.subheadline = dueDate
            objectHeader_orderDetail.description = order.description
            objectHeader_orderDetail.setTag(
                getString(com.sap.mobile.mahlwerk.R.string.status, order.orderStatus.getLocalizedString(requireContext())),
                0
            )

            orderEventAdapter = OrderEventAdapter(requireContext())
            timelineView_orderDetail.adapter = orderEventAdapter
            orderEventAdapter.apply {
                addTimelineCellClickListener { _, position ->
                    onSelectCell(order, position)
                }
                items = order.orderEvents.sortedWith(
                    compareBy(
                        { it.date.year },
                        { it.date.month },
                        { it.date.day },
                        { it.date.hour },
                        { it.date.minute },
                        { it.date.second }
                    )
                ).toMutableList()
            }

            editText_orderDetail_message.setOnEditorActionListener { _, _, _ ->
                sendMessage(order)
                true
            }

            editText_orderDetail_message.setOnTouchListener(OnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= editText_orderDetail_message.right
                        - editText_orderDetail_message
                            .compoundDrawables[2]
                            .bounds.width()
                    ) {
                        sendMessage(order)
                        return@OnTouchListener true
                    }
                }
                false
            })
        })
    }

    private fun onSelectCell(order: Order, position: Int) {
        val event = orderEventAdapter.items[position]

        when (event.orderEventType) {
            OrderEventType.Create -> navigateToCustomer(order)
            OrderEventType.Open,
            OrderEventType.InProgress,
            OrderEventType.Done -> navigateToTaskDetail(order)
            OrderEventType.ReportSent,
            OrderEventType.ReportApproved -> navigateToFinalReport(order)
            OrderEventType.Message -> {
            }
        }
    }

    /**
     * Navigates to the CustomerFragment and displays the customer from the selected order
     *
     * @param order the currently displayed order
     */
    private fun navigateToCustomer(order: Order) {
        mainViewModel.selectedCustomer.value = order.customer
        findNavController().navigate(
            com.sap.mobile.mahlwerk.R.id.action_orderDetailFragment_to_customerFragment
        )
    }

    /**
     * Navigates to the TaskDetailFragment and displays the task associated with the order
     *
     * @param order the currently displayed order
     */
    private fun navigateToTaskDetail(order: Order) {
        viewModel.loadProperties(order, Order.task)
        mainViewModel.navigateToTaskDetail(order.task)
    }

    /**
     * Navigates to the FinalReportFragment and displays the final report of the displayed order
     *
     * @param order the currently displayed order
     */
    private fun navigateToFinalReport(order: Order) {
        viewModel.loadProperties(order, Order.task)
        viewModel.loadProperties(
            order.task,
            Task.job,
            query = DataQuery().expand(Job.materialPosition)
        )
        mainViewModel.navigateToFinalReport(order.task)
    }

    /**
     * Creates a OrderEvent message and sends it to the customer
     *
     * @param order the current displayed order associated with the message
     */
    private fun sendMessage(order: Order) {
        val message = editText_orderDetail_message.text.toString()
        editText_orderDetail_message.text.clear()
        viewModel.sendMessage(message, order)
    }
}
