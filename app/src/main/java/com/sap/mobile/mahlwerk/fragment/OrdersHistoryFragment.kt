package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sap.cloud.android.odata.odataservice.Order
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.OrderAdapter
import com.sap.mobile.mahlwerk.extension.orderStatus
import com.sap.mobile.mahlwerk.extension.setupActionBar
import com.sap.mobile.mahlwerk.model.OrderStatus
import com.sap.mobile.mahlwerk.screen.OrderScreen
import com.sap.mobile.mahlwerk.util.SearchHandler
import kotlinx.android.synthetic.main.fragment_orders_history.*
import kotlinx.android.synthetic.main.item_header.view.*

/**
 * Displays all finished orders in a list
 */
class OrdersHistoryFragment : Fragment(), OrderScreen, SearchHandler {
    private val historyAdapter by lazy {
        OrderAdapter(requireContext()).apply { onItemClick = { navigateToOrderDetail(it) } }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_orders_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar(toolbar_ordersHistory, getString(R.string.ordersHistory))

        view_ordersHistory_history.textView_itemHeader.text = getString(R.string.ordersHistory)
        recyclerView_ordersHistory.adapter = historyAdapter

        observeOrders()
    }

    override fun onPause() {
        super.onPause()
        setHasOptionsMenu(false)
        activity?.invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu_orders_history, menu)

        setupSearchView<Order>(
            activity = requireActivity(),
            scrollView = nestedScrollView_ordersHistory,
            menuItem = menu.findItem(R.id.menu_ordersHistory_search),
            recyclerViews = listOf(Pair(view_ordersHistory_history, recyclerView_ordersHistory))
        )
    }

    /**
     * Observes all orders and displays the finished orders in the view
     */
    private fun observeOrders() {
        viewModel.orders.observe(this, Observer<List<Order>> { orders ->
            orders.forEach {
                viewModel.loadProperties(it, Order.customer)
            }

            historyAdapter.items = orders.filter {
                it.orderStatus == OrderStatus.Done
            }.toMutableList()
        })
    }

    /**
     * Navigates to the OrderDetailFragment and displays the selected order
     *
     * @param order the selected order to be displayed in the detail fragment
     */
    private fun navigateToOrderDetail(order: Order) {
        viewModel.selectedOrder.value = order
        findNavController().navigate(R.id.action_ordersHistoryFragment_to_orderDetailFragment)
    }
}
