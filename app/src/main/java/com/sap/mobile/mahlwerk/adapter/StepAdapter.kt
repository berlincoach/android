package com.sap.mobile.mahlwerk.adapter

import android.content.Context
import android.view.View
import com.sap.cloud.android.odata.odataservice.Step
import com.sap.mobile.mahlwerk.R
import kotlinx.android.synthetic.main.item_step.*

/**
 * Adapter for the Steps in the JobDetailFragment
 */
class StepAdapter(context: Context) : GenericAdapter<Step>(context) {
    override val noItemsString = context.getString(R.string.noItems_step)

    /**
     * ViewHolder used to bind a step with the view
     */
    inner class StepViewHolder(
        view: View
    ) : ViewHolder<Step>(view) {

        override fun bind(data: Step) {
            textView_itemStep_title.text = data.name
        }
    }

    override fun getViewHolder(view: View, viewType: Int): ViewHolder<Step> {
        return StepViewHolder(view)
    }

    override fun getLayoutId(position: Int, item: Step): Int {
        return R.layout.item_step
    }
}