package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.sap.cloud.android.odata.odataservice.Order
import com.sap.cloud.android.odata.odataservice.Task
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.FinalReportAdapter
import com.sap.mobile.mahlwerk.extension.finalReportStatus
import com.sap.mobile.mahlwerk.extension.setupActionBar
import com.sap.mobile.mahlwerk.screen.TaskScreen
import kotlinx.android.synthetic.main.fragment_final_report.*
import kotlinx.android.synthetic.main.item_header.view.*

/**
 * This fragment displays the final report
 */
class FinalReportFragment : Fragment(), TaskScreen {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_final_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view_final_report.textView_itemHeader.text = getString(R.string.jobs)
        observeTask()
    }

    /**
     * Observes the task to create the final report and binds to the view
     */
    private fun observeTask() {
        viewModel.selectedTask.observe(this, Observer<Task> { task ->
            viewModel.loadProperties(task, Task.order, Task.job)
            viewModel.loadProperties(task.order, Order.customer)

            setupActionBar(toolbar_finalReport, getString(R.string.finalReport))

            objectHeader_finalReport.headline = task.order.title
            objectHeader_finalReport.subheadline = task.order.customer.companyName
            objectHeader_finalReport.setTag(
                getString(
                    R.string.status, task.finalReportStatus.getLocalizedString(requireContext())
                ),
                0
            )

            textView_finalReport_sum.text = getString(R.string.sum)
                .format(task.job.sumByDouble { (it.actualWorkHours ?: 0).toDouble() })

            FinalReportAdapter(requireContext()).apply {
                items = task.job
                recyclerView_final_report.adapter = this
            }
        })
    }
}