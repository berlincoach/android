package com.sap.mobile.mahlwerk.extension

import com.sap.cloud.android.odata.odataservice.OrderEvents
import com.sap.mobile.mahlwerk.model.OrderEventType

/** Convenience property for representing the status of a OrderEvent */
val OrderEvents.orderEventType: OrderEventType
    get() {
        return OrderEventType.values()[orderEventTypeID.toInt()]
    }