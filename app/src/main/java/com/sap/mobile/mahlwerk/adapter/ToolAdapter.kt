package com.sap.mobile.mahlwerk.adapter

import android.content.Context
import android.view.View
import com.sap.cloud.android.odata.odataservice.Tool
import com.sap.mobile.mahlwerk.R
import kotlinx.android.synthetic.main.item_tool.*

/**
 * Adapter for the materials in the JobDetailFragment
 */
class ToolAdapter(context: Context) : GenericAdapter<Tool>(context) {
    override val noItemsString = context.getString(R.string.noItems_tool)

    /**
     * ViewHolder used to bind a tool with the view
     */
    inner class ToolViewHolder(
        view: View
    ) : GenericAdapter<Tool>.ViewHolder<Tool>(view) {

        override fun bind(data: Tool) {
            textView_itemTool_title.text = data.name
        }
    }

    override fun getViewHolder(view: View, viewType: Int): ViewHolder<Tool> {
        return ToolViewHolder(view)
    }

    override fun getLayoutId(position: Int, item: Tool): Int {
        return R.layout.item_tool
    }
}