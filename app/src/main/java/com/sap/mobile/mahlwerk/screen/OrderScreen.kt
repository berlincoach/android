package com.sap.mobile.mahlwerk.screen

import androidx.lifecycle.ViewModelProviders
import com.sap.mobile.mahlwerk.viewmodel.OrderViewModel

/**
 * Provides convenience access to the OrderViewModel
 */
interface OrderScreen : Screen {
    /** The ViewModel for the orders tab */
    val viewModel: OrderViewModel
        get() = ViewModelProviders.of(
            requireActivity(),
            mahlwerkApplication.viewModelFactory
        )[OrderViewModel::class.java]
}