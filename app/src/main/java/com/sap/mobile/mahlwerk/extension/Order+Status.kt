package com.sap.mobile.mahlwerk.extension

import com.sap.cloud.android.odata.odataservice.Order
import com.sap.mobile.mahlwerk.model.OrderStatus

/** Convenience property for representing the status of an Order */
val Order.orderStatus: OrderStatus
    get() {
        return OrderStatus.values()[orderStatusID.toInt()]
    }