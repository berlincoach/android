package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.sap.cloud.android.odata.odataservice.Job
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.StepAdapter
import com.sap.mobile.mahlwerk.screen.TaskScreen
import kotlinx.android.synthetic.main.fragment_job_steps.*
import kotlinx.android.synthetic.main.item_header.view.*

/**
 * This fragment displays the steps of a job inside the JobDetailFragment
 */
class JobStepsFragment : Fragment(), TaskScreen {
    /** The adapter holding the steps of the job */
    private val stepAdapter by lazy { StepAdapter(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_job_steps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view_job_steps.textView_itemHeader.text = getString(R.string.steps)
        recyclerView_job_steps.adapter = stepAdapter

        observeJob()
    }

    /**
     * Observes the selected job and binds its steps to the view
     */
    private fun observeJob() {
        viewModel.selectedJob.observe(this, Observer<Job> { job ->
            viewModel.loadProperties(job, Job.step)
            stepAdapter.items = job.step.toMutableList()
        })
    }
}
