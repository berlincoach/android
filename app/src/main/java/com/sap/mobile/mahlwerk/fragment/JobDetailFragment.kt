package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.sap.cloud.android.odata.odataservice.Job
import com.sap.cloud.android.odata.odataservice.Task
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.JobDetailPagerAdapter
import com.sap.mobile.mahlwerk.extension.jobStatus
import com.sap.mobile.mahlwerk.extension.setupActionBar
import com.sap.mobile.mahlwerk.extension.taskStatus
import com.sap.mobile.mahlwerk.model.JobStatus
import com.sap.mobile.mahlwerk.model.TaskStatus
import com.sap.mobile.mahlwerk.screen.TaskScreen
import kotlinx.android.synthetic.main.fragment_job_detail.*

/**
 * This fragment is the container fragment for the job steps and materials
 */
class JobDetailFragment : Fragment(), TaskScreen {
    /** Status icon shown in the menu bar to change the status of the job */
    private lateinit var statusItem: MenuItem

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_job_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPager_jobDetail.adapter = JobDetailPagerAdapter(requireContext(), childFragmentManager)
        tabLayout_jobDetail.setupWithViewPager(viewPager_jobDetail)

        observeJob()
    }

    override fun onPause() {
        super.onPause()
        setHasOptionsMenu(false)
        activity?.invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu_job_detail, menu)
        statusItem = menu.findItem(R.id.menu_jobDetail_status)

        viewModel.selectedJob.observe(this, Observer<Job> { job ->
            statusItem.isVisible = job.jobStatus != JobStatus.Done
                && job.task.taskStatus == TaskStatus.Active
            statusItem.setOnMenuItemClickListener {
                onChangeStatus(job)
                true
            }
        })
    }

    /**
     * Observes the selected job and binds the information to the view
     */
    private fun observeJob() {
        viewModel.selectedJob.observe(this, Observer<Job> { job ->
            viewModel.loadProperties(job, Job.task)
            viewModel.loadProperties(job.task, Task.machine)

            setupActionBar(toolbar_jobDetail, job.title)

            objectHeader_jobDetail.headline = getString(R.string.machine)
                .format(job.task.machine.name)
            objectHeader_jobDetail.subheadline = getString(R.string.predictedTime)
                .format(job.predictedWorkHours.toDouble())
            objectHeader_jobDetail.setTag(
                getString(R.string.status, job.jobStatus.getLocalizedString(requireContext())),
                0
            )
        })
    }

    /**
     * Changes the status of the job to the next status
     *
     * @param job the job to change the status
     */
    private fun onChangeStatus(job: Job) {
        val nextStatus = job.jobStatus.nextStatus.getLocalizedString(requireContext())

        AlertDialog.Builder(requireContext())
            .setTitle(R.string.changeStatusTitle)
            .setMessage(getString(R.string.changeStatus).format(nextStatus))
            .setPositiveButton(R.string.yes) { _, _ ->
                viewModel.onChangeStatus(job)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}
