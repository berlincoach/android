package com.sap.mobile.mahlwerk.logon

import android.content.Context

import com.sap.cloud.mobile.foundation.authentication.OAuth2Configuration

/**
 * This class provides the OAuth configuration object for the application.
 *
 */
object SAPOAuthConfigProvider {

    private val OAUTH_REDIRECT_URL = "https://sapconnect-coach-com-sap-mobile-mahlwerk.cfapps.eu10.hana.ondemand.com"
    private val OAUTH_CLIENT_ID = "6aa27d4e-52ab-4d77-aa2b-d5b594a7dad6"
    private val AUTH_END_POINT = "https://sapconnect-coach-com-sap-mobile-mahlwerk.cfapps.eu10.hana.ondemand.com/oauth2/api/v1/authorize"
    private val TOKEN_END_POINT = "https://sapconnect-coach-com-sap-mobile-mahlwerk.cfapps.eu10.hana.ondemand.com/oauth2/api/v1/token"

    @JvmStatic fun getOAuthConfiguration(context: Context): OAuth2Configuration {

        return OAuth2Configuration.Builder(context)
            .clientId(OAUTH_CLIENT_ID)
            .responseType("code")
            .authUrl(AUTH_END_POINT)
            .tokenUrl(TOKEN_END_POINT)
            .redirectUrl(OAUTH_REDIRECT_URL)
            .build()
    }


}
