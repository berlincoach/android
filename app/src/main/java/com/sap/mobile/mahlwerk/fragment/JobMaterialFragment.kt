package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.sap.cloud.android.odata.odataservice.Job
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.MaterialAdapter
import com.sap.mobile.mahlwerk.adapter.ToolAdapter
import com.sap.mobile.mahlwerk.screen.TaskScreen
import com.sap.mobile.mahlwerk.util.MaterialOrganizer
import kotlinx.android.synthetic.main.fragment_job_material.*
import kotlinx.android.synthetic.main.item_header.view.*

/**
 * This fragment displays the tools and materials of a job inside the JobDetailFragment
 */
class JobMaterialFragment : Fragment(), TaskScreen {
    /** The adapter holding the tools of the job */
    private val toolAdapter by lazy { ToolAdapter(requireContext()) }

    /** The adapter holding the materials of the job */
    private val materialAdapter by lazy { MaterialAdapter(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_job_material, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view_job_tools.textView_itemHeader.text = getString(R.string.tools)
        view_job_materials.textView_itemHeader.text = getString(R.string.materials)

        recyclerView_job_tools.adapter = toolAdapter
        recyclerView_job_materials.adapter = materialAdapter

        observeJob()
    }

    /**
     * Observes the selected job and binds its tools and materials to the view
     */
    private fun observeJob() {
        viewModel.selectedJob.observe(this, Observer<Job> { job ->
            val organizer = MaterialOrganizer(viewModel)
            organizer.addFromJob(job)

            toolAdapter.items = organizer.sortedTools
            materialAdapter.items = organizer.sortedMaterials
        })
    }
}
