package com.sap.mobile.mahlwerk.model

import android.content.Context
import com.sap.mobile.mahlwerk.R

/**
 * Represents the status of an order event as an enum for convenience
 */
enum class OrderEventType(private val resourceId: Int, private val colorId: Int? = null) {
    Create(R.string.type_create),
    Open(R.string.type_open, R.color.status_open),
    InProgress(R.string.type_inProgress, R.color.status_inProgress),
    ReportSent(R.string.type_reportSent),
    ReportApproved(R.string.type_reportApproved),
    Done(R.string.type_done, R.color.status_done),
    Message(R.string.type_message);

    /**
     * Returns the localized string for the status
     *
     * @return the localized string
     */
    fun getLocalizedString(context: Context): String {
        return context.getString(resourceId)
    }

    /**
     * Returns the color for the status
     *
     * @return the color of the status
     */
    fun getColor(context: Context): Int? {
        return context.getColor(colorId ?: return null)
    }
}