package com.sap.mobile.mahlwerk.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.sap.cloud.android.odata.odataservice.OdataService
import com.sap.cloud.android.odata.odataservice.OdataServiceMetadata
import com.sap.cloud.android.odata.odataservice.Order
import com.sap.cloud.android.odata.odataservice.OrderEvents
import com.sap.cloud.mobile.odata.LocalDateTime
import com.sap.mobile.mahlwerk.model.OrderEventType
import com.sap.mobile.mahlwerk.repository.OrderRepository

/**
 * ViewModel used for the orders tab
 */
class OrderViewModel(
    application: Application,
    odataService: OdataService
) : ViewModel(application, odataService) {
    /** Repository containing all orders */
    private val orderRepository = mahlwerkApplication.repositoryFactory
        .getRepository(OdataServiceMetadata.EntitySets.orderSet) as OrderRepository

    /** Orders of the repository as live data */
    val orders = orderRepository.observableEntities

    /** The recent selected order in the orders tab */
    val selectedOrder = MutableLiveData<Order>()

    override fun refresh() {
        orderRepository.read()
    }

    override fun initialRead() {
        initialRead(orderRepository)
    }

    /**
     * Creates an OrderEvent message for the order
     *
     * @param message the string message the user wrote
     * @param order the order associated with the message
     */
    fun sendMessage(message: String, order: Order) {
        if (message.isEmpty()) {
            return
        }

        val orderEvents = OrderEvents(false)
        orderEvents.orderEventTypeID = OrderEventType.Message.ordinal.toLong()
        orderEvents.text = message
        orderEvents.date = LocalDateTime.now()
        orderEvents.orderID = order.orderID
        orderEvents.order = order

        odataService.createEntity(orderEvents)
        selectedOrder.value = selectedOrder.value
    }
}