package com.sap.mobile.mahlwerk.adapter

import android.content.Context
import android.view.View
import com.sap.cloud.android.odata.odataservice.Order
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.extension.orderStatus
import kotlinx.android.synthetic.main.item_order.*

/**
 * Adapter for the OrdersFragment
 */
class OrderAdapter(context: Context) : SearchableAdapter<Order>(context) {
    override val noItemsString = context.getString(R.string.noItems_order)

    /**
     * ViewHolder used to bind a order with the view
     */
    inner class OrderViewHolder(
        view: View
    ) : GenericAdapter<Order>.ViewHolder<Order>(view) {

        override fun bind(data: Order) {
            textView_itemOrder_title.text = data.title
            textView_itemOrder_description.text = data.customer.companyName
            textView_itemOrder_status.text = data.orderStatus.getLocalizedString(context)
            textView_itemOrder_status.setTextColor(data.orderStatus.getColor(context))
        }
    }

    override fun getViewHolder(view: View, viewType: Int): ViewHolder<Order> {
        return OrderViewHolder(view)
    }

    override fun getLayoutId(position: Int, item: Order): Int {
        return R.layout.item_order
    }

    override fun filter(item: Order, text: String): Boolean {
        return item.title.contains(text, true)
    }
}