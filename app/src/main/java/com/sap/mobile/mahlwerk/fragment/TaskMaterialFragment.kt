package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.sap.cloud.android.odata.odataservice.Task
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.MaterialAdapter
import com.sap.mobile.mahlwerk.adapter.ToolAdapter
import com.sap.mobile.mahlwerk.screen.TaskScreen
import com.sap.mobile.mahlwerk.util.MaterialOrganizer
import kotlinx.android.synthetic.main.fragment_task_material.*
import kotlinx.android.synthetic.main.item_header.view.*

/**
 * This fragment displays the tools materials of a task inside the TaskDetailFragment
 */
class TaskMaterialFragment : Fragment(), TaskScreen {
    /** The adapter holding the tools of the task */
    private val toolAdapter by lazy { ToolAdapter(requireContext()) }

    /** The adapter holding the materials of the task */
    private val materialAdapter by lazy { MaterialAdapter(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task_material, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view_task_tools.textView_itemHeader.text = getString(R.string.tools)
        view_task_materials.textView_itemHeader.text = getString(R.string.materials)

        recyclerView_task_tools.adapter = toolAdapter
        recyclerView_task_materials.adapter = materialAdapter

        observeTask()
    }

    /**
     * Observes the selected task and binds its tools and materials to the view
     */
    private fun observeTask() {
        viewModel.selectedTask.observe(this, Observer<Task> { task ->
            val organizer = MaterialOrganizer(viewModel)
            organizer.addFromTask(task)

            toolAdapter.items = organizer.sortedTools
            materialAdapter.items = organizer.sortedMaterials
        })
    }
}
