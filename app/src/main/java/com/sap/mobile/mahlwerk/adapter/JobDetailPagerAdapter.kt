package com.sap.mobile.mahlwerk.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.fragment.JobMaterialFragment
import com.sap.mobile.mahlwerk.fragment.JobStepsFragment

/**
 * Adapter for the JobDetailFragment
 */
class JobDetailPagerAdapter(
    private val context: Context,
    manager: FragmentManager
) : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return JobStepsFragment()
            1 -> return JobMaterialFragment()
        }

        throw IllegalArgumentException("No item found for position: $position")
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return context.getString(R.string.steps)
            1 -> return context.getString(R.string.materials)
        }

        throw IllegalArgumentException("No title found for position: $position")
    }

    override fun getCount(): Int {
        return 2
    }
}