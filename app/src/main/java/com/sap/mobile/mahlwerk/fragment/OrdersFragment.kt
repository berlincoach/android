package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sap.cloud.android.odata.odataservice.Order
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.OrderAdapter
import com.sap.mobile.mahlwerk.extension.orderStatus
import com.sap.mobile.mahlwerk.extension.setupActionBar
import com.sap.mobile.mahlwerk.model.OrderStatus
import com.sap.mobile.mahlwerk.screen.OrderScreen
import com.sap.mobile.mahlwerk.screen.Refreshable
import com.sap.mobile.mahlwerk.util.SearchHandler
import kotlinx.android.synthetic.main.fragment_orders.*
import kotlinx.android.synthetic.main.item_footer.*
import kotlinx.android.synthetic.main.item_header.view.*

/**
 * This fragment provides access to all orders
 */
class OrdersFragment : Fragment(), OrderScreen, Refreshable, SearchHandler {
    /** The adapter containing the open orders*/
    private val orderAdapter by lazy {
        OrderAdapter(requireContext()).apply {
            onItemClick = { navigateToOrderDetail(it) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_orders, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupActionBar(
            toolbar_orders,
            getString(R.string.orders),
            R.drawable.ic_account_circle_white_24dp
        )

        setupRefreshLayout(swipeRefreshLayout_orders, viewModel)

        toolbar_orders.setNavigationOnClickListener {
            findNavController().navigate(R.id.action_ordersFragment_to_profileFragment)
        }

        view_orders_openOrders.textView_itemHeader.text = getString(R.string.openOrders)
        view_orders_history.apply {
            textView_itemFooter.text = getString(R.string.ordersHistory)
            setOnClickListener {
                findNavController().navigate(R.id.action_ordersFragment_to_ordersHistoryFragment)
            }
        }

        recyclerView_orders_openOrders.adapter = orderAdapter

        mainViewModel.onSelectOrder = {
            viewModel.selectedOrder.value = it
        }

        observeOrders()
    }

    override fun onResume() {
        super.onResume()
        viewModel.syncData()
    }

    override fun onPause() {
        super.onPause()
        setHasOptionsMenu(false)
        activity?.invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu_orders, menu)

        setupSearchView<Order>(
            activity = requireActivity(),
            scrollView = nestedScrollView_orders,
            menuItem = menu.findItem(R.id.menu_orders_search),
            recyclerViews = listOf(
                Pair(view_orders_openOrders, recyclerView_orders_openOrders)
            ),
            viewsToHide = listOf(view_orders_history)
        )
    }

    /**
     * Observes all orders and displays the open or scheduled orders in the view
     */
    private fun observeOrders() {
        swipeRefreshLayout_orders.isRefreshing = true

        viewModel.orders.observe(this, Observer<List<Order>> { orders ->
            swipeRefreshLayout_orders.isRefreshing = false

            orders.forEach {
                viewModel.loadProperties(it, Order.customer)
            }

            orderAdapter.items = orders.filter {
                it.orderStatus == OrderStatus.Open || it.orderStatus == OrderStatus.InProgress
            }.toMutableList()
        })
    }

    /**
     * Navigates to the OrderDetailFragment and displays the selected order
     *
     * @param order the selected order to be displayed in the detail fragment
     */
    private fun navigateToOrderDetail(order: Order) {
        viewModel.selectedOrder.value = order
        findNavController().navigate(R.id.action_ordersFragment_to_orderDetailFragment)
    }
}
