package com.sap.mobile.mahlwerk.model

import android.content.Context
import com.sap.mobile.mahlwerk.R

/**
 * Represents the status of an order as an enum for convenience
 */
enum class OrderStatus(private val stringId: Int, private val colorId: Int) {
    New(R.string.status_new, R.color.status_new),
    Open(R.string.status_open, R.color.status_open),
    InProgress(R.string.status_inProgress, R.color.status_inProgress),
    Done(R.string.status_done, R.color.status_done);

    /** Convenience property to get the next status of an order */
    val nextStatus: OrderStatus
        get() = when (this) {
            New -> Open
            Open -> InProgress
            InProgress -> Done
            Done -> throw AssertionError("Status 'Done' has no next status")
        }

    /** Convenience property to get the next statusID of an order */
    val nextStatusID: Long
        get() = nextStatus.ordinal.toLong()

    /**
     * Returns the localized string for the status
     *
     * @return the localized string
     */
    fun getLocalizedString(context: Context): String {
        return context.getString(stringId)
    }

    /**
     * Returns the color for the status
     *
     * @return the color of the status
     */
    fun getColor(context: Context): Int {
        return context.getColor(colorId)
    }
}