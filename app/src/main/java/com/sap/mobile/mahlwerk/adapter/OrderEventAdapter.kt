package com.sap.mobile.mahlwerk.adapter

import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Build
import com.sap.cloud.android.odata.odataservice.OrderEvents
import com.sap.cloud.mobile.fiori.timelineview.TimelineCellProvider
import com.sap.cloud.mobile.fiori.timelineview.views.TimelineCellView
import com.sap.cloud.mobile.fiori.timelineview.views.TimelineDetailedCellView
import com.sap.cloud.mobile.fiori.timelineview.views.TimelineMarkerCellView
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.extension.orderEventType
import com.sap.mobile.mahlwerk.model.OrderEventType

/**
 * Adapter for the Timeline in the OrderDetailFragment
 */
class OrderEventAdapter(private val context: Context) : TimelineCellProvider<OrderEvents>() {
    /** Items to be displayed in the TimelineView */
    var items = mutableListOf<OrderEvents>()

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun getTimelineCellType(position: Int): TimelineCellView.TimelineCellType {
        return if (items[position].orderEventType == OrderEventType.Message) {
            TimelineCellView.TimelineCellType.DETAILED
        } else {
            TimelineCellView.TimelineCellType.DETAILED
        }
    }

    override fun onBindView(cell: TimelineCellView, position: Int, p2: Int) {
        val orderEvent = items[position]
        val markerCell = cell as? TimelineMarkerCellView
            ?: throw IllegalStateException("Timelinecell must be of type TimelineMarkerCellView")

        when (orderEvent.orderEventType) {
            OrderEventType.Message -> {
                val detailedCell = cell as? TimelineDetailedCellView
                    ?: throw IllegalStateException(
                        "Timelinecell must be of type TimelineDetailedCellView"
                    )
                detailedCell.headline = context.getString(R.string.message)
                detailedCell.subHeadline = orderEvent.text
                detailedCell.setHeadlineMargin(25, 25, 25, 5)
                detailedCell.setSubHeadlineMargin(25, 5, 25, 50)
                detailedCell.setHeadlineTextAppearance(
                    R.style.TextAppearance_Fiori_ObjectCell_Headline
                )
            }
            else -> {
                markerCell.headline = orderEvent.orderEventType.getLocalizedString(context)
                markerCell.setHeadlineMargin(25, 25, 25, 25)
            }
        }

        markerCell.setState(TimelineCellView.TimelineCellState.INACTIVE)
        markerCell.event = context.getString(R.string.monthDay)
            .format(orderEvent.date.month, orderEvent.date.day)

        orderEvent.orderEventType.getColor(context)?.let { color ->
            context.getDrawable(R.drawable.ic_brightness_1_white_24dp)?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    it.mutate().colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
                } else {
                    @Suppress("DEPRECATION")
                    it.mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
                }

                markerCell.setBarNodeDrawable(it)
            }
        }
    }
}