package com.sap.mobile.mahlwerk.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.sap.cloud.android.odata.odataservice.Task
import com.sap.mobile.mahlwerk.R
import com.sap.mobile.mahlwerk.adapter.MaterialAdapter
import com.sap.mobile.mahlwerk.adapter.ToolAdapter
import com.sap.mobile.mahlwerk.extension.taskStatus
import com.sap.mobile.mahlwerk.layout.NonScrollableLinearLayoutManager
import com.sap.mobile.mahlwerk.model.TaskStatus
import com.sap.mobile.mahlwerk.screen.TaskScreen
import com.sap.mobile.mahlwerk.util.MaterialOrganizer
import kotlinx.android.synthetic.main.fragment_materials.*

/**
 * This fragment displays the tools and materials of a task in a small dialog
 */
class MaterialsFragment : DialogFragment(), TaskScreen {
    /** The adapter holding the tools of the tasks */
    private val toolAdapter by lazy { ToolAdapter(requireContext()) }

    /** The adapter holding the materials of the tasks */
    private val materialAdapter by lazy { MaterialAdapter(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_materials, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textView_material_tools.text = getString(R.string.tools)
        textView_material_materials.text = getString(R.string.materials)

        val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)

        recyclerView_material_tools.apply {
            addItemDecoration(divider)
            layoutManager = NonScrollableLinearLayoutManager(context)
            adapter = toolAdapter
        }

        recyclerView_material_materials.apply {
            addItemDecoration(divider)
            layoutManager = NonScrollableLinearLayoutManager(context)
            adapter = materialAdapter
        }

        imageView_material_close.setOnClickListener { dismiss() }
        observeTasks()
    }

    /**
     * Observes a tasks and binds its tools and materials to the view
     */
    private fun observeTasks() {
        viewModel.tasks.observe(this, Observer<List<Task>> { tasks ->
            val organizer = MaterialOrganizer(viewModel)
            organizer.addFromTasks(tasks.filter {
                it.taskStatus == TaskStatus.Active || it.taskStatus == TaskStatus.Scheduled
            })

            toolAdapter.items = organizer.sortedTools
            materialAdapter.items = organizer.sortedMaterials
        })
    }
}
